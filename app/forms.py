from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField
from wtforms.validators import DataRequired


class TaskForm(FlaskForm):
    todo = StringField('Task:', validators=[DataRequired()])
    submit = SubmitField('Add')


class EditForm(FlaskForm):
    edit = StringField('edit task:', validators=[DataRequired()])
    submit = SubmitField('update')