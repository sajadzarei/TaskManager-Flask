from flask import redirect, render_template, url_for, flash, request
from app.forms import TaskForm, EditForm
from app.models import Tasks
from app import db, app


@app.route('/', methods=['GET','POST'])
def index():
    form = TaskForm()
    incomplete = Tasks.query.filter_by(status=False).all()
    complete = Tasks.query.filter_by(status=True).all()
    return render_template(
        'index.html',incomplete=incomplete,form=form, complete=complete)


@app.route('/add', methods=['POST'])
def add():
    form = TaskForm()
    todo = Tasks(task=form.todo.data, status=False)
    db.session.add(todo)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/complete/<id>')
def complete(id):
    todo = Tasks.query.filter_by(id=int(id)).first()
    todo.status = True
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/delete/<id>')
def delete(id):
    todo = Tasks.query.filter_by(id=int(id)).first()
    db.session.delete(todo)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/delete_all')
def delete_all():
    todo = Tasks.query.filter_by(status=True).all()
    for i in todo:
        db.session.delete(i)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/edit/<id>', methods=['GET','POST'])
def edit(id):
    form = EditForm()
    todo = Tasks.query.filter_by(id=int(id)).first()
    if request.method == 'POST':
        todo.task = form.edit.data
        db.session.commit()
        return redirect(url_for('index'))
    return render_template('edit.html',todo=todo, form=form)


@app.route('/undo/<id>')
def undo(id):
    todo = Tasks.query.filter_by(id=int(id)).first()
    todo.status = False
    db.session.commit()
    return redirect(url_for('index'))